#!/bin/bash
apt-get update
apt-get install -qq -y software-properties-common
add-apt-repository -y ppa:ubuntu-toolchain-r/test
apt-get update
apt-get install -qq -y gcc-8 g++-8

# update-alternatives --remove-all gcc
# update-alternatives --remove-all g++
# update-alternatives --remove-all cc
# update-alternatives --remove-all c++

update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 10
update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 20

update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7 10
update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 20

update-alternatives --install /usr/bin/cc cc /usr/bin/gcc 30
update-alternatives --set cc /usr/bin/gcc

update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++ 30
update-alternatives --set c++ /usr/bin/g++
