#include <rqt_bag_exporter/topic_types.hpp>

namespace rqt_bag_exporter
{

bool isImageTopic(const std::string topic_type)
{
  if (topic_type == "sensor_msgs/Image" || topic_type == "sensor_msgs/CompressedImage")
    return true;

  return false;
}

bool isCsvWritableTopic(const std::string topic_type)
{
  if (topic_type.find("std_msgs/Bool") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/Duration") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/Float32") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/Float64") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/Int16") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/Int32") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/Int64") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/Int8") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/String") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/Time") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/UInt16") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/UInt32") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/UInt64") != std::string::npos)
    return true;

  if (topic_type.find("std_msgs/UInt8") != std::string::npos)
    return true;

  return false;
}

}
