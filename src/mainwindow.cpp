#include <rqt_bag_exporter/mainwindow.hpp>

namespace rqt_bag_exporter
{

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent)
{
  setWindowTitle("ROS bag exporter");
  QApplication::setWindowIcon(QIcon("://rqt_bag_exporter.png"));
  parent = new QWidget;
  setCentralWidget(parent);
  QHBoxLayout *layout(new QHBoxLayout);
  parent->setLayout(layout);
  connect(this, &MainWindow::enable, this, &MainWindow::setEnabled);

  QMenu *menu(new QMenu("File"));
  menuBar()->addMenu(menu);

  QAction *open_bag(new QAction("Open", menu));
  open_bag->setToolTip("Open a bag file");
  connect(open_bag, &QAction::triggered, this, &MainWindow::openBagFile);
  menu->addAction(open_bag);
  QAction *export_topics(new QAction("Export", menu));
  export_topics->setToolTip("Export topics to files");
  connect(export_topics, &QAction::triggered, this, &MainWindow::exportToDirectory);
  menu->addAction(export_topics);

  QVBoxLayout *left = new QVBoxLayout;
  left->addWidget(new QLabel("Topics to be exported"));

  // Scroll area
  QVBoxLayout *scroll_widget_layout = new QVBoxLayout();
  QWidget *scroll_widget = new QWidget;
  scroll_widget->setLayout(scroll_widget_layout);
  QScrollArea *scroll_area = new QScrollArea;
  scroll_area->setWidget(scroll_widget);
  scroll_area->setWidgetResizable(true);
  scroll_area->setFrameShape(QFrame::NoFrame);

  progress_dialog_ = new QProgressDialog;
  connect(this, &MainWindow::updateProgressDialogValue, progress_dialog_, &QProgressDialog::setValue);
  connect(this, &MainWindow::updateProgressDialogText, progress_dialog_, &QProgressDialog::setLabelText);
  connect(progress_dialog_, &QProgressDialog::canceled, this, &MainWindow::progressDialogCancelled);
  progress_dialog_->setModal(true);
  progress_dialog_->reset(); // Otherwise it will show up after minimumDuration

  topics_container_ = new QWidget;
  scroll_widget_layout->addWidget(topics_container_);
  left->addWidget(scroll_area);

  QHBoxLayout *buttons_layout(new QHBoxLayout);
  all_ = new QPushButton("All");
  connect(all_, &QPushButton::clicked, [ = ]()
  {
    QList<QCheckBox *> checkboxes = topics_container_->findChildren<QCheckBox *>();

    for (int i(0); i < checkboxes.size(); ++i)
      if (checkboxes.at(i)->isEnabled())
        checkboxes.at(i)->setChecked(true);
  }
         );

  none_ = new QPushButton("None");
  connect(none_, &QPushButton::clicked, [ = ]()
  {
    QList<QCheckBox *> checkboxes = topics_container_->findChildren<QCheckBox *>();

    for (int i(0); i < checkboxes.size(); ++i)
      if (checkboxes.at(i)->isEnabled())
        checkboxes.at(i)->setChecked(false);
  }
         );

  all_->setEnabled(false);
  none_->setEnabled(false);
  buttons_layout->addWidget(all_);
  buttons_layout->addWidget(none_);
  left->addLayout(buttons_layout);

  QVBoxLayout *right(new QVBoxLayout);
  csv_one_file_ = new QCheckBox("One file for all CSV topics");
  csv_one_file_->setChecked(true);

  QHBoxLayout *decimal_separator_layout(new QHBoxLayout);
  decimal_separator_ = new QComboBox;
  decimal_separator_->addItem("Point");
  decimal_separator_->addItem("Comma");
  decimal_separator_->setCurrentIndex(1);
  decimal_separator_layout->addWidget(new QLabel("Decimal separator: "));
  decimal_separator_layout->addWidget(decimal_separator_);
  right->addWidget(csv_one_file_);
  right->addLayout(decimal_separator_layout);
  right->addStretch(1);

  right->addWidget(new QLabel("<b>Start / end time<b>"));
  start_end_time_ = new QWidget;
  time_begin_ = new QDoubleSpinBox;
  time_begin_->setEnabled(false);
  time_end_ = new QDoubleSpinBox;
  time_end_->setEnabled(false);
  QHBoxLayout *start_end_time(new QHBoxLayout);
  start_end_time->addWidget(time_begin_);
  start_end_time->addWidget(new QLabel(" sec"));
  start_end_time->addWidget(time_end_);
  start_end_time->addWidget(new QLabel(" sec"));
  start_end_time_->setLayout(start_end_time);
  right->addWidget(start_end_time_);
  right->addSpacing(30);

  video_tab_ = new QTabWidget;
  QWidget *first_tab(new QWidget);
  QVBoxLayout *tab_information(new QVBoxLayout);
  QLabel *observed_fps(new QLabel);
  QHBoxLayout *video_fps(new QHBoxLayout);
  QSpinBox *fps(new QSpinBox);
  fps->setEnabled(false);
  video_fps->addWidget(new QLabel("Video FPS"));
  video_fps->addWidget(fps);
  QHBoxLayout *observed_fps_layout(new QHBoxLayout);
  tab_information->addLayout(observed_fps_layout);
  observed_fps_layout->addWidget(new QLabel("Observed FPS"));
  observed_fps_layout->addWidget(observed_fps);
  tab_information->addLayout(video_fps);
  first_tab->setLayout(tab_information);
  video_tab_->addTab(first_tab, "Topic");
  right->addWidget(video_tab_);
  right->addStretch(1);

  layout->addLayout(left);
  layout->addLayout(right);

  connect(time_begin_, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::updateTimeEnd);
  connect(time_end_, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::updateTimeBegin);

  qRegisterMetaType<QMessageBox::Icon>();
  connect(this, &MainWindow::displayMessageBox, this, &MainWindow::displayMessageBoxHandler);

  load();
  csv_ac_.reset(new ExportToCsvActionClient("export_csv", true));
  merged_csv_ac_.reset(new ExportToMergedCsvActionClient("export_to_one_csv", true));
  video_ac_.reset(new ExportToVideoActionClient("export_video", true));
  connect(this, &MainWindow::finishedCurrentDataExport, this, &MainWindow::exportNextTopic);
}

MainWindow::~MainWindow()
{
}

void MainWindow::closeBag()
{
  ros::ServiceClient client = nh_.serviceClient<rqt_bag_exporter::CloseBag>("close_bag");
  rqt_bag_exporter::CloseBag srv;

  if (!client.waitForExistence(ros::Duration(1)))
  {
    if (!nh_.ok())
      return;

    Q_EMIT displayMessageBox("Service error",
                             QString::fromStdString("Service server " + client.getService() + " does not exist."),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (!client.call(srv))
  {
    if (!nh_.ok())
      return;

    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Failed to call service " + client.getService()),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (!srv.response.error.empty())
  {
    if (!nh_.ok())
      return;

    Q_EMIT displayMessageBox(QString::fromStdString(client.getService() + " failed"),
                             QString::fromStdString(srv.response.error),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }
}

void MainWindow::openBagFile()
{
  const std::string file_name(QFileDialog::getOpenFileName(this, "Open bag file",
                              settings_->value("open_path").toString(),
                              "Bag files (*.bag)", Q_NULLPTR).toStdString());

  if (file_name.empty())
    return;

  std::string path_file(file_name);
  const int pos(path_file.find_last_of("/"));
  path_file.erase(pos, std::string::npos);
  settings_->setValue("open_path", QString::fromStdString(path_file));
  directory_ = path_file;
  ros::ServiceClient client = nh_.serviceClient<rqt_bag_exporter::OpenBag>("open_bag");

  if (!client.waitForExistence(ros::Duration(1)))
  {
    Q_EMIT displayMessageBox("Service error",
                             QString::fromStdString("Service server " + client.getService() + " does not exist."),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  rqt_bag_exporter::OpenBag srv;
  srv.request.bag_file = file_name;

  if (!client.call(srv))
  {
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Failed to call service " + client.getService()),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (!srv.response.error.empty())
  {
    Q_EMIT displayMessageBox(QString::fromStdString(client.getService() + " failed"),
                             QString::fromStdString(srv.response.error),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  listTopicsInBagFile();
  displayTopics();
  fillStartEndTime();
  video_tab_->clear();

  for (std::size_t i(0); i < list_.topics.size(); ++i)
  {
    if (list_.topics.at(i).type == "sensor_msgs/CompressedImage" ||
        list_.topics.at(i).type == "sensor_msgs/Image")
      fillVideoTab(list_.topics.at(i).name);
  }
}

void MainWindow::listTopicsInBagFile()
{
  ros::ServiceClient client = nh_.serviceClient<rqt_bag_exporter::ListTopics>("list_topics");
  rqt_bag_exporter::ListTopics srv;

  if (!client.waitForExistence(ros::Duration(1)))
  {
    Q_EMIT displayMessageBox("Service error",
                             QString::fromStdString("Service server " + client.getService() + " does not exist."),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (!client.call(srv))
  {
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Failed to call service " + client.getService()),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (!srv.response.error.empty())
  {
    Q_EMIT displayMessageBox(QString::fromStdString(client.getService() + " failed"),
                             QString::fromStdString(srv.response.error),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  list_ = srv.response;

  if (list_.topics.empty())
  {
    all_->setEnabled(false);
    none_->setEnabled(false);
  }
  else
  {
    all_->setEnabled(true);
    none_->setEnabled(true);
  }
}

void MainWindow::displayTopics()
{
  QVBoxLayout *checkbox_layout(new QVBoxLayout);
  qDeleteAll(topics_container_->children());
  topics_container_->setLayout(checkbox_layout);

  std::sort(list_.topics.begin(), list_.topics.end(), [](rqt_bag_exporter::Topic &a,
                                                         rqt_bag_exporter::Topic &b)
  {
    return a.name < b.name;
  });

  for (std::size_t i(0); i < list_.topics.size(); ++i)
  {
    const bool check_csv_type = isCsvWritableTopic(list_.topics.at(i).type);
    const bool check_video_type = isImageTopic(list_.topics.at(i).type);
    QCheckBox *checkbox = new QCheckBox(QString::fromStdString(list_.topics.at(i).name));
    checkbox->setObjectName(QString::fromStdString(list_.topics.at(i).name));
    checkbox->setToolTip(QString::fromStdString(list_.topics.at(i).type));

    if (!check_csv_type && !check_video_type)
      checkbox->setEnabled(false);

    checkbox_layout->addWidget(checkbox);
  }
}

void MainWindow::fillStartEndTime()
{
  ros::ServiceClient client = nh_.serviceClient<rqt_bag_exporter::GetDuration>("get_duration");
  rqt_bag_exporter::GetDuration srv;

  if (!client.waitForExistence(ros::Duration(1)))
  {
    Q_EMIT displayMessageBox("Service error",
                             QString::fromStdString("Service server " + client.getService() + " does not exist."),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (!client.call(srv))
  {
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Failed to call service " + client.getService()),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (!srv.response.error.empty())
  {
    Q_EMIT displayMessageBox(QString::fromStdString(client.getService() + " failed"),
                             QString::fromStdString(srv.response.error),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (srv.response.duration == 0)
  {
    Q_EMIT displayMessageBox(QString::fromStdString(client.getService()),
                             "Video duration is zero",
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  time_begin_->setEnabled(true);
  time_end_->setEnabled(true);
  time_begin_->setRange(0, srv.response.duration);
  time_end_->setRange(0, srv.response.duration);
  time_end_->setValue(srv.response.duration);
}

void MainWindow::fillVideoTab(const std::string topic_name)
{
  ros::ServiceClient client = nh_.serviceClient<rqt_bag_exporter::EstimateVideoFps>(
                                "estimate_video_fps");
  rqt_bag_exporter::EstimateVideoFps srv;
  srv.request.topic_name = topic_name;

  if (!client.waitForExistence(ros::Duration(1)))
  {
    Q_EMIT displayMessageBox("Service error",
                             QString::fromStdString("Service server " + client.getService() + " does not exist."),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (!client.call(srv))
  {
    Q_EMIT displayMessageBox("Error calling service",
                             QString::fromStdString("Failed to call service " + client.getService()),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (srv.response.fps == 0)
  {
    Q_EMIT displayMessageBox("Video topic error", "Fail to get video frame rate of rosbag", "",
                             QMessageBox::Icon::Critical);
    return;
  }

  fps_ = srv.response.fps;
  QWidget *tab(new QWidget);
  QVBoxLayout *tab_information(new QVBoxLayout);
  QLabel *observed_fps(new QLabel);
  observed_fps->setText(QString::number(srv.response.fps));
  QHBoxLayout *video_fps(new QHBoxLayout);
  QSpinBox *fps(new QSpinBox);
  fps->setMaximum(999);
  fps->setValue(srv.response.fps);
  video_fps->addWidget(new QLabel("Video FPS"));
  video_fps->addWidget(fps);
  QHBoxLayout *observed_fps_layout(new QHBoxLayout);
  tab_information->addLayout(observed_fps_layout);
  observed_fps_layout->addWidget(new QLabel("Observed FPS"));
  observed_fps_layout->addWidget(observed_fps);
  tab_information->addLayout(video_fps);
  tab->setLayout(tab_information);
  video_tab_->addTab(tab, QString::fromStdString(srv.request.topic_name));

  connect(fps, qOverload<int>(&QSpinBox::valueChanged), this, &MainWindow::updateFps);
}

void MainWindow::updateFps(const int fps)
{
  fps_ = fps;
}

void MainWindow::updateTimeEnd(const double time_begin)
{
  if (time_begin >= time_end_->value())
    time_end_->setValue(time_begin + 0.01);
}

void MainWindow::updateTimeBegin(const double time_end)
{
  if (time_end <= time_begin_->value())
    time_begin_->setValue(time_end - 0.01);
}

void MainWindow::exportToDirectory()
{
  fillTopicsToExport();

  if (to_export_.topics.empty())
  {
    Q_EMIT displayMessageBox("Zero topic to export", "Choose at least one topic to be exported", "",
                             QMessageBox::Icon::Warning);
    return;
  }

  directory_ =
    QFileDialog::getExistingDirectory(
      this,
      "Export directory",
      settings_->value("path_directory").toString(),
      QFileDialog::ShowDirsOnly).toStdString();

  if (directory_.empty())
    return;

  settings_->setValue("path_directory", QString::fromStdString(directory_));
  Q_EMIT enable(false);
  exportNextTopic();
}

void MainWindow::exportNextTopic()
{
  if (to_export_.topics.empty())
  {
    Q_EMIT enable(true);
    return;
  }

  progress_dialog_->reset();
  progress_dialog_->show();

  const rqt_bag_exporter::Topic t(to_export_.topics.back());

  bool is_csv_exportable = isCsvWritableTopic(t.type);
  bool is_video_exportable = isImageTopic(t.type);

  if (is_csv_exportable)
  {
    if (csv_one_file_->isChecked())
    {
      rqt_bag_exporter::ListTopicsResponse::_topics_type csv_topics;
      // Find all Csv exportable topics, add them in csv_topics and remove them from to_export_
      for (auto it(to_export_.topics.begin()); it != to_export_.topics.end();)
      {
        if (isCsvWritableTopic(it->type))
        {
          csv_topics.emplace_back(*it);
          to_export_.topics.erase(it);
        }
        else
          ++it;
      }

      if (!merged_csv_ac_->waitForServer(ros::Duration(1)))
      {
        Q_EMIT displayMessageBox("Action error", "Action server to export one Csv does not exist.", "",
                                 QMessageBox::Icon::Critical);
        return;
      }

      std::vector<std::string> csv_topics_names;
      for (auto t : csv_topics)
        csv_topics_names.emplace_back(t.name);

      merged_csv_goal_.directory = directory_;
      merged_csv_goal_.topics_name = csv_topics_names;
      merged_csv_goal_.start_time = time_begin_->value();
      merged_csv_goal_.end_time = time_end_->value();
      merged_csv_goal_.comma_decimal_separator = (decimal_separator_->currentIndex() == 1 ? true : false);
      action_type_ = MERGED_Csv;
      progress_dialog_->setWindowTitle("CSV file");
      progress_dialog_->reset();
      progress_dialog_->show();

      merged_csv_ac_->sendGoal(merged_csv_goal_,
                               boost::bind(&MainWindow::mergedCsvDoneCb, this, _1, _2),
                               NULL,
                               boost::bind(&MainWindow::mergedCsvFeedbackCb, this, _1));
      QtConcurrent::run(this, &MainWindow::mergedCsvAction);
      return;
    }

    to_export_.topics.pop_back();
    if (!csv_ac_->waitForServer(ros::Duration(1)))
    {
      Q_EMIT displayMessageBox("Action error", "Action server to export Csv does not exist.", "",
                               QMessageBox::Icon::Critical);
      return;
    }

    csv_goal_.directory = directory_;
    csv_goal_.topic_name = t.name;
    csv_goal_.start_time = time_begin_->value();
    csv_goal_.end_time = time_end_->value();
    csv_goal_.comma_decimal_separator = (decimal_separator_->currentIndex() == 1 ? true : false);
    action_type_ = CSV;
    progress_dialog_->setWindowTitle("CSV files");
    progress_dialog_->reset();
    progress_dialog_->show();

    csv_ac_->sendGoal(csv_goal_,
                      boost::bind(&MainWindow::csvDoneCb, this, _1, _2),
                      NULL,
                      boost::bind(&MainWindow::csvFeedbackCb, this, _1));
    QtConcurrent::run(this, &MainWindow::csvAction);
  }
  else if (is_video_exportable)
  {
    to_export_.topics.pop_back();
    if (!video_ac_->waitForServer(ros::Duration(1)))
    {
      Q_EMIT displayMessageBox("Action error", "Action server to export video does not exist.", "",
                               QMessageBox::Icon::Critical);
      return;
    }

    video_goal_.directory = directory_;
    video_goal_.topic_name = t.name;
    video_goal_.start_time = time_begin_->value();
    video_goal_.end_time = time_end_->value();
    video_goal_.fps = fps_;
    action_type_ = VIDEO;
    progress_dialog_->setWindowTitle("Video files");

    video_ac_->sendGoal(video_goal_,
                        boost::bind(&MainWindow::videoDoneCb, this, _1, _2),
                        NULL,
                        boost::bind(&MainWindow::videoFeedbackCb, this, _1));
    QtConcurrent::run(this, &MainWindow::videoAction);
  }
  else
    Q_EMIT finishedCurrentDataExport();
}

void MainWindow::csvAction()
{
  csv_ac_->waitForResult();
  Q_EMIT finishedCurrentDataExport();
}

void MainWindow::mergedCsvAction()
{
  merged_csv_ac_->waitForResult();
  Q_EMIT finishedCurrentDataExport();
}

void MainWindow::videoAction()
{
  video_ac_->waitForResult();
  Q_EMIT finishedCurrentDataExport();
}

void MainWindow::fillTopicsToExport()
{
  to_export_.topics.clear();
  QList<QCheckBox *> checkboxes = topics_container_->findChildren<QCheckBox *>();

  for (int i(0); i < checkboxes.size(); ++i)
  {
    if (checkboxes.at(i)->isChecked())
    {
      rqt_bag_exporter::Topic t;
      t.name = checkboxes.at(i)->objectName().toStdString();
      t.type = checkboxes.at(i)->toolTip().toStdString();
      to_export_.topics.emplace_back(t);
    }
  }
}

void MainWindow::csvDoneCb(const actionlib::SimpleClientGoalState &state,
                           const rqt_bag_exporter::ExportToCsvResultConstPtr &result)
{
  if (state.state_ == actionlib::SimpleClientGoalState::SUCCEEDED)
  {
    if (!result->error.empty())
    {
      Q_EMIT progress_dialog_->canceled();
      Q_EMIT displayMessageBox("Action ExportToCsv returned an error: ",
                               QString::fromStdString(result->error),
                               "",
                               QMessageBox::Icon::Critical);
      return;
    }
  }

  if (state.state_ == actionlib::SimpleClientGoalState::ABORTED)
  {
    Q_EMIT progress_dialog_->canceled();
    Q_EMIT displayMessageBox("Action ExportToCsv aborted with error: ",
                             QString::fromStdString(result->error),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (state.state_ == actionlib::SimpleClientGoalState::PREEMPTED)
  {
    Q_EMIT progress_dialog_->canceled();
    return;
  }

  Q_EMIT progress_dialog_->accepted();
}

void MainWindow::csvFeedbackCb(const rqt_bag_exporter::ExportToCsvFeedbackConstPtr &feedback)
{
  if (progress_dialog_->wasCanceled())
    return;

  Q_EMIT updateProgressDialogText(QString::fromStdString(feedback->progress_msg));
  Q_EMIT updateProgressDialogValue(feedback->progress_value);
}

void MainWindow::mergedCsvDoneCb(const actionlib::SimpleClientGoalState &state,
                                 const rqt_bag_exporter::ExportToMergedCsvResultConstPtr &result)
{
  if (state.state_ == actionlib::SimpleClientGoalState::SUCCEEDED)
  {
    if (!result->error.empty())
    {
      Q_EMIT progress_dialog_->canceled();
      Q_EMIT displayMessageBox("Action ExportToMergedCsv returned an error: ",
                               QString::fromStdString(result->error),
                               "",
                               QMessageBox::Icon::Critical);
      return;
    }
  }

  if (state.state_ == actionlib::SimpleClientGoalState::ABORTED)
  {
    Q_EMIT progress_dialog_->canceled();
    Q_EMIT displayMessageBox("Action ExportToMergedCsv aborted with error: ",
                             QString::fromStdString(result->error),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (state.state_ == actionlib::SimpleClientGoalState::PREEMPTED)
  {
    Q_EMIT progress_dialog_->canceled();
    return;
  }

  Q_EMIT progress_dialog_->accepted();
}

void MainWindow::mergedCsvFeedbackCb(const rqt_bag_exporter::ExportToMergedCsvFeedbackConstPtr &feedback)
{
  if (progress_dialog_->wasCanceled())
    return;

  Q_EMIT updateProgressDialogText(QString::fromStdString(feedback->progress_msg));
  Q_EMIT updateProgressDialogValue(feedback->progress_value);
}

void MainWindow::videoDoneCb(const actionlib::SimpleClientGoalState &state,
                             const rqt_bag_exporter::ExportToVideoResultConstPtr &result)
{
  if (state.state_ == actionlib::SimpleClientGoalState::SUCCEEDED)
  {
    if (!result->error.empty())
    {
      Q_EMIT progress_dialog_->canceled();
      Q_EMIT displayMessageBox("Action ExportToVideo returned an error: ",
                               QString::fromStdString(result->error),
                               "",
                               QMessageBox::Icon::Critical);
      return;
    }
  }

  if (state.state_ == actionlib::SimpleClientGoalState::ABORTED)
  {
    Q_EMIT progress_dialog_->canceled();
    Q_EMIT displayMessageBox("Action ExportToVideo aborted with error: ",
                             QString::fromStdString(result->error),
                             "",
                             QMessageBox::Icon::Critical);
    return;
  }

  if (state.state_ == actionlib::SimpleClientGoalState::PREEMPTED)
  {
    Q_EMIT progress_dialog_->canceled();
    return;
  }

  Q_EMIT progress_dialog_->accepted();
}

void MainWindow::videoFeedbackCb(const rqt_bag_exporter::ExportToVideoFeedbackConstPtr &feedback)
{
  if (progress_dialog_->wasCanceled())
    return;

  Q_EMIT updateProgressDialogText(QString::fromStdString(feedback->progress_msg));
  Q_EMIT updateProgressDialogValue(feedback->progress_value);
}

void MainWindow::progressDialogCancelled()
{
  if (action_type_ == CSV)
  {
    if (csv_ac_->getState().state_ == actionlib::SimpleClientGoalState::ACTIVE)
      csv_ac_->cancelAllGoals();
  }
  else if (action_type_ == MERGED_Csv)
  {
    if (merged_csv_ac_->getState().state_ == actionlib::SimpleClientGoalState::ACTIVE)
      merged_csv_ac_->cancelAllGoals();
  }
  else if (action_type_ == VIDEO)
  {
    if (video_ac_->getState().state_ == actionlib::SimpleClientGoalState::ACTIVE)
      video_ac_->cancelAllGoals();
  }

  to_export_.topics.clear();
  progress_dialog_->reset();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
  closeBag();
  // Save
  settings_ = new QSettings("Institut Maupertuis", "rqt_bag_exporter");
  settings_->setValue("csv_one_file", csv_one_file_->isChecked());
  settings_->setValue("decimal_separator", decimal_separator_->currentIndex());
  settings_->setValue("geometry", saveGeometry());
  settings_->setValue("window_state", saveState());
  QMainWindow::closeEvent(event);
}

void MainWindow::load()
{
  settings_ = new QSettings("Institut Maupertuis", "rqt_bag_exporter");
  csv_one_file_->setChecked(settings_->value("csv_one_file", csv_one_file_->isChecked()).toBool());
  csv_one_file_->setChecked(settings_->value("csv_one_file", csv_one_file_->isChecked()).toBool());
  decimal_separator_->setCurrentIndex(settings_->value("decimal_separator").toInt());
  restoreGeometry(settings_->value("geometry").toByteArray());
  restoreState(settings_->value("window_state").toByteArray());
}

void MainWindow::displayMessageBoxHandler(const QString title,
    const QString text,
    const QString info,
    const QMessageBox::Icon icon)
{
  const bool old_state(isEnabled());
  setEnabled(false);
  QMessageBox msg_box;
  msg_box.setWindowTitle(title);
  msg_box.setText(text);
  msg_box.setInformativeText(info);
  msg_box.setIcon(icon);
  msg_box.setStandardButtons(QMessageBox::Ok);
  msg_box.exec();
  setEnabled(old_state);
}

}
