#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-copy"
#include <actionlib/client/simple_action_client.h>
#pragma GCC diagnostic pop
#include <filesystem>
#include <fstream>
#include <iostream>
#include <regex>
#include <rosbag/bag.h>
#include <ros/ros.h>
#include <rqt_bag_exporter/CloseBag.h>
#include <rqt_bag_exporter/EstimateVideoFps.h>
#include <rqt_bag_exporter/ExportToCsvAction.h>
#include <rqt_bag_exporter/ExportToMergedCsvAction.h>
#include <rqt_bag_exporter/ExportToVideoAction.h>
#include <rqt_bag_exporter/GetDuration.h>
#include <rqt_bag_exporter/ListTopics.h>
#include <rqt_bag_exporter/OpenBag.h>
#include <rqt_bag_exporter/topic_types.hpp>

// Note: regexes MUST match the whole string!

enum ColorCode
{
  FG_BLACK = 30,
  FG_RED = 31,
  FG_GREEN = 32,
  FG_YELLOW = 33,
  FG_BLUE = 34,
  FG_MAGENTA = 35,
  FG_CYAN = 36,
  FG_LIGHT_GRAY = 37,
  FG_DEFAULT = 39,
  BG_RED = 41,
  BG_GREEN = 42,
  BG_BLUE = 44,
  BG_DEFAULT = 49,
  FG_DARK_GRAY = 90,
  FG_LIGHT_RED = 91,
  FG_LIGHT_GREEN = 92,
  FG_LIGHT_YELLOW = 93,
  FG_LIGHT_BLUE = 94,
  FG_LIGHT_MAGENTA = 95,
  FG_LIGHT_CYAN = 96,
  FG_WHITE = 97
};

std::ostream &operator<<(std::ostream &os,
                         ColorCode code)
{
  return os << "\033[" << static_cast<int>(code) << "m";
}

bool convertTextFile(const std::string file_path,
                     std::vector<std::string> &files_list)
{
  std::ifstream file(file_path);
  std::string line;

  try
  {
    if (file.is_open())
    {
      while (std::getline(file, line))
        files_list.push_back(line);
      file.close();
    }
  }
  catch (const std::ifstream::failure &e)
  {
    ROS_ERROR_STREAM("Could not convert the list of files: " << e.what());
    return false;
  }
  return true;
}

bool openBagFile(const std::string file_path,
                 ros::ServiceClient &client)
{
  if (!client.waitForExistence(ros::Duration(1)))
  {
    ROS_ERROR_STREAM("Service error: " + client.getService() + " does not exist");
    return false;
  }

  rqt_bag_exporter::OpenBag srv;
  srv.request.bag_file = file_path;
  if (!client.call(srv))
  {
    ROS_ERROR_STREAM("Failed to call service " + client.getService());
    return false;
  }

  if (!srv.response.error.empty())
  {
    ROS_ERROR_STREAM("Call to service " << client.getService() + " returned an error: " << srv.response.error);
    return false;
  }

  return true;
}

bool closeBagFile(ros::ServiceClient &client)
{
  if (!client.waitForExistence(ros::Duration(1)))
  {
    ROS_ERROR_STREAM("Service error: " + client.getService() + " does not exist");
    return false;
  }

  rqt_bag_exporter::CloseBag srv;
  if (!client.call(srv))
  {
    ROS_ERROR_STREAM("Failed to call service " + client.getService());
    return false;
  }

  if (!srv.response.error.empty())
  {
    ROS_ERROR_STREAM("Call to service " << client.getService() + " returned an error: " << srv.response.error);
    return false;
  }

  return true;
}

bool listTopics(rqt_bag_exporter::ListTopicsResponse &list, ros::ServiceClient &client)
{
  if (!client.waitForExistence(ros::Duration(1)))
  {
    ROS_ERROR_STREAM("Service error: " + client.getService() + " does not exist");
    return false;
  }

  rqt_bag_exporter::ListTopics srv;
  if (!client.call(srv))
  {
    ROS_ERROR_STREAM("Failed to call service " + client.getService());
    return false;
  }

  if (!srv.response.error.empty())
  {
    ROS_ERROR_STREAM("Call to service " << client.getService() + " returned an error: " << srv.response.error);
    return false;
  }

  list = srv.response;
  return true;
}

bool getDuration(double &duration,
                 ros::ServiceClient &client)
{
  if (!client.waitForExistence(ros::Duration(1)))
  {
    ROS_ERROR_STREAM("Service error: " + client.getService() + " does not exist");
    return false;
  }

  rqt_bag_exporter::GetDuration srv;
  if (!client.call(srv))
  {
    ROS_ERROR_STREAM("Failed to call service " + client.getService());
    return false;
  }

  if (!srv.response.error.empty())
  {
    ROS_ERROR_STREAM("Call to service " << client.getService() + " returned an error: " << srv.response.error);
    return false;
  }

  duration = srv.response.duration;
  return true;
}

bool estimateVideoFps(const std::string &video_name,
                      uint64_t &estimated_fps,
                      ros::ServiceClient &client)
{
  if (!client.waitForExistence(ros::Duration(1)))
  {
    ROS_ERROR_STREAM("Service error: " + client.getService() + " does not exist");
    return false;
  }

  rqt_bag_exporter::EstimateVideoFps srv;
  srv.request.topic_name = video_name;

  if (!client.call(srv))
  {
    ROS_ERROR_STREAM("Failed to call service " + client.getService());
    return false;
  }

  if (!srv.response.error.empty())
  {
    ROS_ERROR_STREAM("Call to service " << client.getService() + " returned an error: " << srv.response.error);
    return false;
  }

  estimated_fps = srv.response.fps;
  return true;
}

void mergedCSVdoneCb(const actionlib::SimpleClientGoalState &,
                     const rqt_bag_exporter::ExportToMergedCsvResultConstPtr &result)
{
  if (!result->error.empty())
    ROS_INFO_STREAM("Error exporting merged CSV: " << result->error);
}

void mergedCSVactiveCb()
{
}

void mergedCSVfeedbackCb(const rqt_bag_exporter::ExportToMergedCsvFeedbackConstPtr &feedback)
{
  ROS_INFO_STREAM(FG_BLUE << "[" << feedback->progress_value << " %] " << FG_DEFAULT << feedback->progress_msg);
}

void oneCSVdoneCb(const actionlib::SimpleClientGoalState &,
                  const rqt_bag_exporter::ExportToCsvResultConstPtr &result)
{
  if (!result->error.empty())
    ROS_INFO_STREAM("Error exporting CSV: " << result->error);
}

void oneCSVactiveCb()
{
}

void oneCSVfeedbackCb(const rqt_bag_exporter::ExportToCsvFeedbackConstPtr &feedback)
{
  ROS_INFO_STREAM(FG_BLUE << "[" << feedback->progress_value << " %] " << FG_DEFAULT << feedback->progress_msg);
}

void videoExportdoneCb(const actionlib::SimpleClientGoalState &,
                       const rqt_bag_exporter::ExportToVideoResultConstPtr &result)
{
  if (!result->error.empty())
    ROS_INFO_STREAM("Error exporting video: " << result->error);
}

void videoExportactiveCb()
{
}

void videoExportfeedbackCb(const rqt_bag_exporter::ExportToVideoFeedbackConstPtr &feedback)
{
  ROS_INFO_STREAM(FG_BLUE << "[" << feedback->progress_value << " %] " << FG_DEFAULT << feedback->progress_msg);
}

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "rqt_bag_exporter_node");
  ros::NodeHandle nh("~");
  ros::ServiceClient client_open_bag = nh.serviceClient<rqt_bag_exporter::OpenBag>("/open_bag");
  ros::ServiceClient client_close_bag = nh.serviceClient<rqt_bag_exporter::CloseBag>("/close_bag");
  ros::ServiceClient client_list_topics = nh.serviceClient<rqt_bag_exporter::ListTopics>("/list_topics");
  ros::ServiceClient client_get_duration = nh.serviceClient<rqt_bag_exporter::GetDuration>("/get_duration");
  ros::ServiceClient client_get_fps_estimation = nh.serviceClient<rqt_bag_exporter::EstimateVideoFps>("/estimate_video_fps");

  actionlib::SimpleActionClient<rqt_bag_exporter::ExportToCsvAction> one_csv_action("export_csv");
  actionlib::SimpleActionClient<rqt_bag_exporter::ExportToMergedCsvAction> merged_csv_action("export_to_one_csv");
  actionlib::SimpleActionClient<rqt_bag_exporter::ExportToVideoAction> video_action("export_video");

  bool merged_csv(true), create_subdir(true), export_video(false);
  std::string topic_name_exclude, topic_type_exclude, topic_name_include, topic_type_include, dir, files_list;
  double topics_duration(0), videos_duration(0);

  nh.getParam("dir", dir);
  nh.getParam("files_list", files_list);
  nh.getParam("topic_name_exclude", topic_name_exclude);
  nh.getParam("topic_name_include", topic_name_include);
  nh.getParam("topic_type_exclude", topic_type_exclude);
  nh.getParam("topic_type_include", topic_type_include);
  nh.getParam("merged_csv", merged_csv);
  nh.getParam("create_subdir", create_subdir);
  nh.getParam("export_video", export_video);
  nh.getParam("topics_duration", topics_duration);
  nh.getParam("videos_duration", videos_duration);

  // Default regexes to match everything
  if (topic_name_include.empty())
    topic_name_include = ".*";
  if (topic_type_include.empty())
    topic_type_include = ".*";

  ROS_INFO_STREAM("Node parameters:");
  ROS_INFO_STREAM("dir = " << dir);
  ROS_INFO_STREAM("files_list = " << files_list);
  ROS_INFO_STREAM("topic_name_exclude = " << topic_name_exclude);
  ROS_INFO_STREAM("topic_name_include = " << topic_name_include);
  ROS_INFO_STREAM("topic_type_exclude = " << topic_type_exclude);
  ROS_INFO_STREAM("topic_type_include = " << topic_type_include);
  ROS_INFO_STREAM("merged_csv = " << (merged_csv ? "yes" : "no"));
  ROS_INFO_STREAM("create_subdir = " << (create_subdir ? "yes" : "no"));
  ROS_INFO_STREAM("export_video = " << (export_video ? "yes" : "no"));
  ROS_INFO_STREAM("topics_duration = " << topics_duration);
  ROS_INFO_STREAM("videos_duration = " << videos_duration << std::endl);

  if (dir.empty() && files_list.empty())
  {
    ROS_ERROR_STREAM("dir or files_list param value must be non-empty");
    return 1;
  }
  if (!dir.empty() && !files_list.empty())
  {
    ROS_ERROR_STREAM("dir and files_list params cannot be both set");
    return 1;
  }

  const std::string bag_extension(".bag");
  std::vector<std::string> files_to_process;
  bool is_files_list(!files_list.empty());

  // Regexes creation
  std::regex reg_name_exclude, reg_type_exclude, reg_name_include, reg_type_include;
  try
  {
    reg_name_exclude = topic_name_exclude;
  }
  catch (const std::regex_error &e)
  {
    ROS_ERROR_STREAM("Invalid regex for topic_name_exclude (" << topic_name_exclude << "): " << e.what());
    return 1;
  }
  try
  {
    reg_type_exclude = topic_type_exclude;
  }
  catch (const std::regex_error &e)
  {
    ROS_ERROR_STREAM("Invalid regex for topic_type_exclude (" << topic_type_exclude << "): " << e.what());
    return 1;
  }
  try
  {
    reg_name_include = topic_name_include;
  }
  catch (const std::regex_error &e)
  {
    ROS_ERROR_STREAM("Invalid regex for topic_name_include (" << topic_name_include << "): " << e.what());
    return 1;
  }
  try
  {
    reg_type_include = topic_type_include;
  }
  catch (const std::regex_error &e)
  {
    ROS_ERROR_STREAM("Invalid regex for topic_type_include (" << topic_type_include << "): " << e.what());
    return 1;
  }

  // Input dir checking
  if (!is_files_list)
  {
    // Exists?
    try
    {
      if (!std::filesystem::exists(dir))
      {
        ROS_ERROR_STREAM("The input directory " << dir << " does not exist");
        return 1;
      }
    }
    catch (const std::filesystem::filesystem_error &e)
    {

      ROS_ERROR_STREAM("Could not check directory " << dir << " existence: " << e.what());
      return 1;
    }

    // Is it a directory?
    try
    {
      if (!std::filesystem::is_directory(dir))
      {
        ROS_ERROR_STREAM(dir << " is not a directory");
        return 1;
      }
    }
    catch (const std::filesystem::filesystem_error &e)
    {
      ROS_ERROR_STREAM("Could not determine file " << dir << " type: " << e.what());
      return 1;
    }

    // Check rights on each file, else delete the file from the files list
    // (if owner has not all rights, failed)
    try
    {
      auto p = std::filesystem::status(dir).permissions();

      if (!(((p & std::filesystem::perms::owner_read) != std::filesystem::perms::none) &&
            ((p & std::filesystem::perms::owner_write) != std::filesystem::perms::none) &&
            ((p & std::filesystem::perms::owner_exec) != std::filesystem::perms::none)))
      {
        ROS_ERROR_STREAM("Cannot read/write inside the directory: " << dir);
        return 1;
      }
    }
    catch (const std::filesystem::filesystem_error &e)
    {
      ROS_ERROR_STREAM("Could not determine file " << dir << " permissions: " << e.what());
      return 1;
    }

    // Do this directory contain any bag files ? Fill the vector with each file path
    for (auto &file : std::filesystem::recursive_directory_iterator(dir))
      if (file.path().extension() == bag_extension)
      {
        try
        {
          files_to_process.emplace_back(std::filesystem::path(file));
        }
        catch (const std::filesystem::filesystem_error &e)
        {
          ROS_ERROR_STREAM("Error listing bag files: " << e.what());
          return 1;
        }
      }
  }

  // Input list of file checking
  // First extract each line and fill the files_to_process vector
  if (!files_list.empty())
    if (!convertTextFile(files_list, files_to_process))
      return 1;

  // For each file listed into the files_to_process vector
  // Check each file extension
  for (auto file_it(files_to_process.begin()); file_it != files_to_process.end(); ++file_it)
  {
    try
    {
      if (std::filesystem::path(*file_it).extension() != bag_extension)
      {
        ROS_WARN_STREAM(*file_it << " is not a bag file, removing it from the list");
        files_to_process.erase(file_it);
      }
    }
    catch (const std::filesystem::filesystem_error &e)
    {
      ROS_ERROR_STREAM("Could not check bag file extensions: " << e.what());
      return 1;
    }
  }

  // Check if bag file exist. Otherwise delete it from the vector
  for (auto file_it(files_to_process.begin()); file_it != files_to_process.end();)
  {
    if (file_it->empty())
      continue;

    try
    {
      if (!exists(std::filesystem::path(*file_it)))
      {
        ROS_WARN_STREAM(*file_it << "does not exist, removing it from the list");
        file_it = files_to_process.erase(file_it);
      }
      else
        ++file_it;
    }
    catch (const std::filesystem::filesystem_error &e)
    {
      ROS_ERROR_STREAM("Could not determine if the bag file exists: " << e.what());
      return 1;
    }
  }

  if (files_to_process.empty())
  {
    if (!dir.empty())
    {
      ROS_ERROR_STREAM("There are no bag files in this directory:" << dir);
      return 1;
    }
    else
    {
      ROS_ERROR_STREAM("There are no bag file name in the list: " << files_list);
      return 1;
    }
  }

  // For each bag file:
  // rqt_bag_exporter/OpenBag : open bag file
  // rqt_bag_exporter/GetDuration : get the duration of the bag file (nota: each bag will be exported entirely)
  // rqt_bag_exporter/ListTopics : list every topics of the bag file
  // rqt_bag_exporter/ExportToMergedCsvAction : export selected topics to the same CSV file
  // rqt_bag_exporter/ExportToCsvAction : export selected topics to distinct csv_topics_names file
  // rqt_bag_exporter/ExportToVideoAction : : export video of the bag file
  // rqt_bag_exporter/CloseBag : close the bag file

  for (auto &file : files_to_process) // File = full path of each file
  {
    std::vector<std::string>::size_type file_index = &file - &files_to_process.at(0) + 1;
    ROS_INFO_STREAM(FG_GREEN  << "[" << file_index << "/" << files_to_process.size() << "] bag file: " << file << FG_DEFAULT);

    // Open bag file
    if (!openBagFile(file, client_open_bag))
    {
      ROS_ERROR_STREAM("Error opening bag file (" << file << "), skipping this file");
      continue;
    }

    // Get the bag duration, only if user does not provide a duration
    if (topics_duration == 0)
    {
      if (!getDuration(topics_duration, client_get_duration))
      {
        ROS_ERROR_STREAM("Could not determine bag file (" << file << ") duration, skipping this file");
        closeBagFile(client_close_bag);
        continue;
      }
    }

    // List every topics
    rqt_bag_exporter::ListTopicsResponse topics_list;
    if (!listTopics(topics_list, client_list_topics))
    {
      ROS_WARN_STREAM("Could not list topics in the file (" << file << "), skipping this file");
      closeBagFile(client_close_bag);
      continue;
    }

    // Filter each bag file topics with regex
    // Topics name
    // Include
    for (auto it(topics_list.topics.begin()); it != topics_list.topics.end();)
    {
      if (std::regex_match(it->name, reg_name_include))
        ++it;
      else
        it = topics_list.topics.erase(it);
    }
    // Exclude
    for (auto it(topics_list.topics.begin()); it != topics_list.topics.end();)
    {
      if (std::regex_match(it->name, reg_name_exclude))
        it = topics_list.topics.erase(it);
      else
        ++it;
    }

    // Topics type
    // Include
    for (auto it(topics_list.topics.begin()); it != topics_list.topics.end();)
    {
      if (std::regex_match(it->type, reg_type_include))
        ++it;
      else
        it = topics_list.topics.erase(it);
    }
    // Exclude
    for (auto it(topics_list.topics.begin()); it != topics_list.topics.end();)
    {
      if (std::regex_match(it->type, reg_type_exclude))
        it = topics_list.topics.erase(it);
      else
        ++it;
    }

    if (topics_list.topics.empty())
    {
      ROS_WARN_STREAM("Zero topics selected, verify your input regexes, skipping this file");
      closeBagFile(client_close_bag);
      continue;
    }

    // Call the action service according to merged_csv input
    std::string export_dir;
    if (create_subdir)
    {
      std::filesystem::path file_path(file);
      try
      {
        if (!std::filesystem::create_directory(file_path.parent_path() / file_path.stem()))
        {
          ROS_INFO_STREAM("Could not create the sub directory " << file_path.stem());
        }
      }
      catch (const std::filesystem::filesystem_error &e)
      {
        ROS_ERROR_STREAM("Could not create the sub directory " << file_path.stem() << ": " << e.what() << ". Skipping this file");
        closeBagFile(client_close_bag);
        continue;
      }

      export_dir = file_path.parent_path() / file_path.stem();
    }
    else
    {
      std::filesystem::path file_path(file);
      export_dir = file_path.parent_path();
    }

    // Split video topics and text data topics
    // check if each topic can be exported
    // and gather every topics's name in order to prepare the goal
    std::vector<std::string> csv_topics_names;
    std::vector<std::string> video_topics_names;
    for (auto &topic : topics_list.topics)
    {
      if (rqt_bag_exporter::isCsvWritableTopic((topic.type)))
        csv_topics_names.emplace_back(topic.name);
      else if (rqt_bag_exporter::isImageTopic(topic.type) && export_video)
        video_topics_names.emplace_back(topic.name);
      else
        ROS_WARN_STREAM("Topic " << topic.name << " (type: " << topic.type << ")" << " cannot be exported");
    }

    // Check if topics lists are empty
    if (csv_topics_names.empty() && video_topics_names.empty())
    {
      ROS_WARN_STREAM("Zero CSV and zero video topics to export, skipping this file");
      continue;
    }

    if (!csv_topics_names.empty())
    {
      if (merged_csv)
      {
        ROS_INFO_STREAM(FG_LIGHT_MAGENTA << "[1/1] merged CSV: all topics" << FG_DEFAULT);
        if (!merged_csv_action.waitForServer(ros::Duration(1)))
        {
          ROS_ERROR_STREAM("Action error: action server to export merged CSV does not exist.");
          return 1;
        }
        rqt_bag_exporter::ExportToMergedCsvGoal merged_csv_goal;
        merged_csv_goal.directory = export_dir;
        merged_csv_goal.topics_name = csv_topics_names;
        merged_csv_goal.start_time = 0.0;
        merged_csv_goal.end_time = topics_duration;

        merged_csv_action.sendGoal(merged_csv_goal, &mergedCSVdoneCb, &mergedCSVactiveCb, &mergedCSVfeedbackCb);
        merged_csv_action.waitForResult(ros::Duration());
      }
      else // Don't merge the CSV
      {
        for (auto &topic_name : csv_topics_names)
        {
          std::vector<std::string>::size_type topic_index = &topic_name - &csv_topics_names.at(0) + 1;
          ROS_INFO_STREAM(FG_MAGENTA << "[" << topic_index << "/" << csv_topics_names.size() << "] CSV topic: " << topic_name << FG_DEFAULT);

          if (!one_csv_action.waitForServer(ros::Duration(1)))
          {
            ROS_ERROR_STREAM("Action error : action server to export one CSV does not exist.");
            return 1;
          }
          rqt_bag_exporter::ExportToCsvGoal one_csv_goal;
          one_csv_goal.directory = export_dir;
          one_csv_goal.topic_name = topic_name;
          one_csv_goal.start_time = 0.0;
          one_csv_goal.end_time = topics_duration;
          one_csv_action.sendGoal(one_csv_goal, &oneCSVdoneCb, &oneCSVactiveCb, &oneCSVfeedbackCb);

          //wait for the action to return in order to wait to open the next bag (...)
          one_csv_action.waitForResult(ros::Duration());
        }
      }
    }
    if (!video_topics_names.empty() && export_video)
    {
      for (auto &video_name : video_topics_names)
      {
        std::vector<std::string>::size_type video_index = &video_name - &video_topics_names.at(0) + 1;
        ROS_INFO_STREAM(FG_LIGHT_MAGENTA << "[" << video_index << "/" << video_topics_names.size() << "] video topic: " << video_name << FG_DEFAULT);
        uint64_t current_fps;
        if (!estimateVideoFps(video_name, current_fps, client_get_fps_estimation))
        {
          ROS_ERROR_STREAM("Could not estimate video FPS");
          closeBagFile(client_close_bag);
          return 1;
        }

        if (!video_action.waitForServer(ros::Duration(1)))
        {
          ROS_ERROR_STREAM("Action error: action server to export videos does not exist.");
          return 1;
        }
        rqt_bag_exporter::ExportToVideoGoal video_goal;
        video_goal.directory = export_dir;
        video_goal.topic_name = video_name;
        video_goal.start_time = 0.0;
        if (videos_duration == 0)
          video_goal.end_time = topics_duration;
        else
          video_goal.end_time = videos_duration;
        video_goal.fps = current_fps;
        video_action.sendGoal(video_goal, &videoExportdoneCb, &videoExportactiveCb, &videoExportfeedbackCb);
        video_action.waitForResult(ros::Duration());
      }
    }

    // Close the bag file
    if (!closeBagFile(client_close_bag))
    {
      ROS_ERROR_STREAM("Error closing the bag file");
      return 1;
    }
  }

  ROS_INFO_STREAM("Done");
  return 0;
}
