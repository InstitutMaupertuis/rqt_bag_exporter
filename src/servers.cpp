#include <actionlib/server/action_server.h>
#include <cv_bridge/cv_bridge.h>
#include <fstream>
#include <locale>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/ros.h>
#include <rqt_bag_exporter/CloseBag.h>
#include <rqt_bag_exporter/EstimateVideoFps.h>
#include <rqt_bag_exporter/ExportToCsvAction.h>
#include <rqt_bag_exporter/ExportToMergedCsvAction.h>
#include <rqt_bag_exporter/ExportToVideoAction.h>
#include <rqt_bag_exporter/GetDuration.h>
#include <rqt_bag_exporter/ListTopics.h>
#include <rqt_bag_exporter/OpenBag.h>
#include <sensor_msgs/CompressedImage.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Duration.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Int8.h>
#include <std_msgs/String.h>
#include <std_msgs/Time.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/UInt64.h>
#include <std_msgs/UInt8.h>
#include <string>

std::shared_ptr<rosbag::Bag> bag;
std::string bag_file_name("");

struct comma_separator : std::numpunct<char>
{
  virtual char do_decimal_point() const override
  {
    return ',';
  }
};

void exportVideo(actionlib::ServerGoalHandle<rqt_bag_exporter::ExportToVideoAction> gh)
{
  gh.setAccepted();
  rqt_bag_exporter::ExportToVideoGoal goal(*gh.getGoal());
  rqt_bag_exporter::ExportToVideoResult result;
  rqt_bag_exporter::ExportToVideoFeedback feedback;

  if (goal.end_time <= goal.start_time)
  {
    result.error = "End time and start time are not valid.";
    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);
  }

  if (!bag || bag->getSize() <= 0)
  {
    result.error = "No valid bag file is opened, call open_bag service to open one.";
    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);
    return;
  }

  std::string file_name(bag_file_name);
  int pos(file_name.find_last_of("."));
  file_name.erase(pos, std::string::npos);
  pos = file_name.find_last_of("/");
  file_name.erase(0, pos + 1);
  std::string topic_name(goal.topic_name);
  pos = topic_name.find_first_of("/");
  topic_name.erase(0, pos + 1);
  std::replace(topic_name.begin(), topic_name.end(), '/', '_');

  std::vector<std::string> topics;
  topics.push_back(goal.topic_name);
  rosbag::View view(*bag, rosbag::TopicQuery(topics));
  if (view.size() == 0)
  {
    result.error = "Topic '" + goal.topic_name + "' doesn't exist in this bag file";
    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);
    return;
  }

  feedback.progress_msg = "Starting video encoding";
  feedback.progress_value = 0;
  gh.publishFeedback(feedback);

  cv::VideoWriter video;

  // Retrieve first image to get image info
  // Open video file
  const std::string video_name(goal.directory + "/" + file_name + "_" + topic_name + ".mkv");

  for (rosbag::MessageInstance const first : view)
  {
    sensor_msgs::Image::ConstPtr img = first.instantiate<sensor_msgs::Image>();
    sensor_msgs::CompressedImage::ConstPtr img_c = first.instantiate<sensor_msgs::CompressedImage>();
    if (!img && !img_c)
      continue;

    try
    {
      // Convert (compressed) image data to cv::Mat
      cv::Mat image;
      if (img_c)
        image = cv::imdecode(img_c->data, 1);

      if (img)
      {
        cv_bridge::CvImagePtr cv_image(cv_bridge::toCvCopy(img, img->encoding));
        image = cv_image->image;
      }

      bool color(true);
      if (image.depth() != CV_8U)
      {
        result.error = "Image format is not supported. Only 8 bit (CV_8U depth) images are supported";
        if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
          gh.setAborted(result);
        return;
      }
      else if (image.type() == CV_8UC1) // 1 channel = grey image
        color = false;

      if (!video.open(video_name,
                      cv::VideoWriter::fourcc('X', '2', '6', '4'),
                      goal.fps,
                      image.size(),
                      color))
      {
        result.error = "Cannot open video file for writing";
        if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
          gh.setAborted(result);
        return;
      }
    }
    catch (cv_bridge::Exception &e)
    {
      result.error = "Could not extract/convert first image";
      if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
        gh.setAborted(result);
      return;
    }
    break;
  }

  ros::Time rosbag_begin(view.getBeginTime());
  const double duration(goal.end_time - goal.start_time);

  for (rosbag::MessageInstance const m : view)
  {
    if (gh.getGoalStatus().status != actionlib_msgs::GoalStatus::ACTIVE)
      return;

    if ((rosbag_begin.toSec() + goal.start_time) > m.getTime().toSec())
      continue;

    if ((rosbag_begin.toSec() + goal.end_time) < m.getTime().toSec())
      break;

    const double time_relative_to_start((m.getTime().sec - rosbag_begin.sec) - goal.start_time);
    const unsigned progress_value = 100 * time_relative_to_start / duration;
    if (feedback.progress_value != progress_value)
    {
      feedback.progress_value = progress_value;
      feedback.progress_msg = "Encoding video";
      gh.publishFeedback(feedback);
    }
    sensor_msgs::Image::ConstPtr img = m.instantiate<sensor_msgs::Image>();
    sensor_msgs::CompressedImage::ConstPtr img_c = m.instantiate<sensor_msgs::CompressedImage>();

    if (!img && !img_c)
      continue;

    try
    {
      cv::Mat image;

      if (img_c)
        image = cv::imdecode(img_c->data, 1);

      if (img)
      {
        cv_bridge::CvImagePtr cv_image(cv_bridge::toCvCopy(img, img->encoding));
        image = cv_image->image;
      }

      video << image;
    }
    catch (cv_bridge::Exception &e)
    {
      result.error = "Could not convert image";
      gh.setAborted(result);
      return;
    }
  }

  feedback.progress_msg = "Writing video file";
  feedback.progress_value = 100;
  gh.publishFeedback(feedback);
  video.release();

  feedback.progress_msg = "Success";
  feedback.progress_value = 100;
  gh.publishFeedback(feedback);
  ros::Duration(0.5).sleep();
  gh.setSucceeded(result);
}

void exportVideoCancelCb(actionlib::ServerGoalHandle<rqt_bag_exporter::ExportToVideoAction> gh)
{
  if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE
      || gh.getGoalStatus().status == actionlib_msgs::GoalStatus::PREEMPTING)
    gh.setCanceled();
}

void exportCsv(actionlib::ServerGoalHandle<rqt_bag_exporter::ExportToCsvAction> gh)
{
  std::string csv_separator(",");

  gh.setAccepted();
  rqt_bag_exporter::ExportToCsvGoal goal;
  rqt_bag_exporter::ExportToCsvResult result;
  rqt_bag_exporter::ExportToCsvFeedback feedback;
  goal = *gh.getGoal();

  if (!bag || bag->getSize() <= 0)
  {
    result.error = "No valid bag file is opened, call open_bag service to open one.";
    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);
    return;
  }

  std::vector<std::string> topics;
  topics.push_back(goal.topic_name);
  rosbag::View view(*bag, rosbag::TopicQuery(topics));
  if (view.size() == 0)
  {
    result.error = "Topic '" + goal.topic_name + "' doesn't exist in this bag file";
    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);
    return;
  }

  feedback.progress_msg = "Starting CSV export";
  feedback.progress_value = 0;
  gh.publishFeedback(feedback);

  std::ofstream csv_file;
  std::string file_name(bag_file_name);
  int pos = file_name.find_last_of(".");
  file_name.erase(pos, std::string::npos);
  pos = file_name.find_last_of("/");
  file_name.erase(0, pos + 1);
  std::string topic_name(goal.topic_name);
  pos = topic_name.find_first_of("/");
  topic_name.erase(0, pos + 1);
  std::replace(topic_name.begin(), topic_name.end(), '/', '_');
  std::string path_file(goal.directory + "/" + file_name + "_" + topic_name + ".csv");
  csv_file.open(path_file);
  if (!csv_file.is_open())
  {
    result.error = "Failed to open CSV file " + path_file;
    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);
    return;
  }

  if (goal.comma_decimal_separator)
  {
    csv_file.imbue(std::locale(std::cout.getloc(), new comma_separator));
    csv_separator = ";";
  }

  csv_file << "Time" << csv_separator << goal.topic_name << std::endl;
  ros::Time rosbag_begin(view.getBeginTime());

  for (rosbag::MessageInstance const m : view)
  {
    if (gh.getGoalStatus().status != actionlib_msgs::GoalStatus::ACTIVE)
      return;
    if ((rosbag_begin.toSec() + goal.start_time) > m.getTime().toSec())
    {
      continue;
    }
    if ((rosbag_begin.toSec() + goal.end_time) < m.getTime().toSec())
    {
      break;
    }
    std::uint64_t real_time(m.getTime().sec - (rosbag_begin.sec + goal.start_time));
    std::uint64_t coeficient(100 / (goal.end_time - goal.start_time));
    std::uint64_t transmit_value(real_time * coeficient);
    std::string transmit_msg("Exporting topic " + goal.topic_name);
    if (transmit_value != feedback.progress_value)
    {
      feedback.progress_msg = transmit_msg;
      feedback.progress_value = transmit_value;
      gh.publishFeedback(feedback);
    }
    double time(m.getTime().toSec() - (rosbag_begin.toSec() + goal.start_time));

    std_msgs::Float32::ConstPtr ft_32 = m.instantiate<std_msgs::Float32>();
    if (ft_32 != nullptr)
      csv_file << time << csv_separator << ft_32->data << std::endl;

    std_msgs::Float64::ConstPtr ft_64 = m.instantiate<std_msgs::Float64>();
    if (ft_64 != nullptr)
      csv_file << time << csv_separator << ft_64->data << std::endl;

    std_msgs::Int8::ConstPtr int_8 = m.instantiate<std_msgs::Int8>();
    if (int_8 != nullptr)
      csv_file << time << csv_separator << int_8->data << std::endl;

    std_msgs::Int16::ConstPtr int_16 = m.instantiate<std_msgs::Int16>();
    if (int_16 != nullptr)
      csv_file << time << csv_separator << int_16->data << std::endl;

    std_msgs::Int32::ConstPtr int_32 = m.instantiate<std_msgs::Int32>();
    if (int_32 != nullptr)
      csv_file << time << csv_separator << int_32->data << std::endl;

    std_msgs::Int64::ConstPtr int_64 = m.instantiate<std_msgs::Int64>();
    if (int_64 != nullptr)
      csv_file << time << csv_separator << int_64->data << std::endl;

    std_msgs::UInt8::ConstPtr uint_8 = m.instantiate<std_msgs::UInt8>();
    if (uint_8 != nullptr)
      csv_file << time << csv_separator << uint_8->data << std::endl;

    std_msgs::UInt16::ConstPtr uint_16 = m.instantiate<std_msgs::UInt16>();
    if (uint_16 != nullptr)
      csv_file << time << csv_separator << uint_16->data << std::endl;

    std_msgs::UInt32::ConstPtr uint_32 = m.instantiate<std_msgs::UInt32>();
    if (uint_32 != nullptr)
      csv_file << time << csv_separator << uint_32->data << std::endl;

    std_msgs::UInt64::ConstPtr uint_64 = m.instantiate<std_msgs::UInt64>();
    if (uint_64 != nullptr)
      csv_file << time << csv_separator << uint_64->data << std::endl;

    std_msgs::Bool::ConstPtr bool_ = m.instantiate<std_msgs::Bool>();
    if (bool_ != nullptr)
      csv_file << time << csv_separator << (bool_->data ? "True" : "False") << std::endl;

    std_msgs::Duration::ConstPtr dur = m.instantiate<std_msgs::Duration>();
    if (dur != nullptr)
      csv_file << time << csv_separator << dur->data << std::endl;

    std_msgs::String::ConstPtr str = m.instantiate<std_msgs::String>();
    if (str != nullptr)
      csv_file << time << csv_separator << str->data << std::endl;

    std_msgs::Time::ConstPtr time_ = m.instantiate<std_msgs::Time>();
    if (time_ != nullptr)
      csv_file << time << csv_separator << time_->data.toSec() << std::endl;
  }

  feedback.progress_msg = "Writing CSV file";
  feedback.progress_value = 100;
  gh.publishFeedback(feedback);

  csv_file.close();
  feedback.progress_msg = "Success";
  feedback.progress_value = 100;
  gh.publishFeedback(feedback);
  ros::Duration(0.25).sleep();

  gh.setSucceeded(result);
  return;
}

void exportCsvCancelCb(actionlib::ServerGoalHandle<rqt_bag_exporter::ExportToCsvAction> gh)
{
  if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE
      || gh.getGoalStatus().status == actionlib_msgs::GoalStatus::PREEMPTING)
    gh.setCanceled();
}

void exportMergedCsv(actionlib::ServerGoalHandle<rqt_bag_exporter::ExportToMergedCsvAction> gh)
{
  std::string csv_separator(",");

  gh.setAccepted();
  rqt_bag_exporter::ExportToMergedCsvGoal goal;
  rqt_bag_exporter::ExportToMergedCsvResult result;
  rqt_bag_exporter::ExportToMergedCsvFeedback feedback;
  goal = *gh.getGoal();

  if (!bag || bag->getSize() <= 0)
  {
    result.error = "No valid bag file is opened, call open_bag service to open one.";

    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);

    return;
  }

  rosbag::View view(*bag, rosbag::TopicQuery(goal.topics_name));
  if (view.size() == 0)
  {
    result.error = "Some topics do not exist in this bag file";

    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);

    return;
  }

  feedback.progress_msg = "Starting CSV export";
  feedback.progress_value = 0;
  gh.publishFeedback(feedback);

  std::ofstream csv_file;
  std::string file_name(bag_file_name);
  int pos = file_name.find_last_of(".");
  file_name.erase(pos, std::string::npos);
  pos = file_name.find_last_of("/");
  file_name.erase(0, pos + 1);
  std::string path_file(goal.directory + "/" + file_name + ".csv");
  csv_file.open(path_file);

  if (!csv_file.is_open())
  {
    result.error = "Failed to open CSV file " + path_file;

    if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE)
      gh.setAborted(result);
    return;
  }

  if (goal.comma_decimal_separator)
  {
    csv_file.imbue(std::locale(std::cout.getloc(), new comma_separator));
    csv_separator = ";";
  }

  using TimeValue = std::pair<std::string, std::string>;
  using TimeValues = std::vector<TimeValue>;
  using TopicsTimeValues = std::map<std::string, TimeValues>;

  feedback.progress_msg = "Opened CSV file";
  feedback.progress_value = 30;
  gh.publishFeedback(feedback);

  ros::Time rosbag_begin(view.getBeginTime());
  TopicsTimeValues topics_time_values;
  for (rosbag::MessageInstance const m : view)
  {
    if (gh.getGoalStatus().status != actionlib_msgs::GoalStatus::ACTIVE)
      return;

    if ((rosbag_begin.toSec() + goal.start_time) > m.getTime().toSec())
      continue;

    if ((rosbag_begin.toSec() + goal.end_time) < m.getTime().toSec())
      break;

    std::stringstream ss;
    if (goal.comma_decimal_separator)
      ss.imbue(std::locale(std::cout.getloc(), new comma_separator));

    double time(m.getTime().toSec() - (rosbag_begin.toSec() + goal.start_time));
    TimeValue tv;
    ss << time;
    tv.first = ss.str();
    ss.str("");
    ss.clear();

    std_msgs::Float32::ConstPtr ft_32 = m.instantiate<std_msgs::Float32>();
    if (ft_32 != nullptr)
    {
      ss << ft_32->data;
      tv.second = ss.str();
    }

    std_msgs::Float64::ConstPtr ft_64 = m.instantiate<std_msgs::Float64>();
    if (ft_64 != nullptr)
    {
      ss << ft_64->data;
      tv.second = ss.str();
    }

    std_msgs::Int8::ConstPtr int_8 = m.instantiate<std_msgs::Int8>();
    if (int_8 != nullptr)
    {
      ss << int_8->data;
      tv.second = ss.str();
    }

    std_msgs::Int16::ConstPtr int_16 = m.instantiate<std_msgs::Int16>();
    if (int_16 != nullptr)
    {
      ss << int_16->data;
      tv.second = ss.str();
    }

    std_msgs::Int32::ConstPtr int_32 = m.instantiate<std_msgs::Int32>();
    if (int_32 != nullptr)
    {
      ss << int_32->data;
      tv.second = ss.str();
    }

    std_msgs::Int64::ConstPtr int_64 = m.instantiate<std_msgs::Int64>();
    if (int_64 != nullptr)
    {
      ss << int_64->data;
      tv.second = ss.str();
    }

    std_msgs::UInt8::ConstPtr uint_8 = m.instantiate<std_msgs::UInt8>();
    if (uint_8 != nullptr)
    {
      ss << uint_8->data;
      tv.second = ss.str();
    }

    std_msgs::UInt16::ConstPtr uint_16 = m.instantiate<std_msgs::UInt16>();
    if (uint_16 != nullptr)
    {
      ss << uint_16->data;
      tv.second = ss.str();
    }

    std_msgs::UInt32::ConstPtr uint_32 = m.instantiate<std_msgs::UInt32>();
    if (uint_32 != nullptr)
    {
      ss << uint_32->data;
      tv.second = ss.str();
    }

    std_msgs::UInt64::ConstPtr uint_64 = m.instantiate<std_msgs::UInt64>();
    if (uint_64 != nullptr)
    {
      ss << uint_64->data;
      tv.second = ss.str();
    }

    std_msgs::Bool::ConstPtr bool_ = m.instantiate<std_msgs::Bool>();
    if (bool_ != nullptr)
      tv.second = bool_->data ? "True" : "False";

    std_msgs::Duration::ConstPtr dur = m.instantiate<std_msgs::Duration>();
    if (dur != nullptr)
    {
      ss << dur->data.toSec();
      tv.second = ss.str();
    }

    std_msgs::String::ConstPtr str = m.instantiate<std_msgs::String>();
    if (str != nullptr)
      tv.second = str->data;

    std_msgs::Time::ConstPtr time_ = m.instantiate<std_msgs::Time>();
    if (time_ != nullptr)
    {
      ss << time_->data.toSec();
      tv.second = ss.str();
    }

    topics_time_values[m.getTopic()].emplace_back(tv);
  }

  feedback.progress_msg = "Converted all topics values to strings";
  feedback.progress_value = 60;
  gh.publishFeedback(feedback);

  if (gh.getGoalStatus().status != actionlib_msgs::GoalStatus::ACTIVE)
    return;

  for (TopicsTimeValues::iterator it(topics_time_values.begin()); it != topics_time_values.end() ; ++it)
    csv_file << "Time" << csv_separator << it->first << csv_separator;
  csv_file << std::endl;

  std::vector<uint64_t> size_vec;
  for (auto &topic : topics_time_values)
    size_vec.emplace_back(topic.second.size());
  uint64_t max_bag_lines = *(std::max_element(size_vec.begin(), size_vec.end()));

  if (gh.getGoalStatus().status != actionlib_msgs::GoalStatus::ACTIVE)
    return;

  uint64_t csv_index_line(0);
  for (std::size_t i(0); ; ++i)
  {
    if (gh.getGoalStatus().status != actionlib_msgs::GoalStatus::ACTIVE)
      return;

    // Check if there is data left to write
    bool data_left(false);
    for (auto topic_tv : topics_time_values)
    {
      if (topic_tv.second.empty())
        continue;
      if (i > topic_tv.second.size() - 1)
        continue;

      data_left = true;
      break;
    }

    if (!data_left)
      break;

    // Loop over all topics and write values
    for (auto topic_tv : topics_time_values)
    {
      if (topic_tv.second.empty())
      {
        csv_file << csv_separator << " " << csv_separator;
        continue;
      }
      if (i > topic_tv.second.size() - 1)
      {
        csv_file << csv_separator << " " << csv_separator;
        continue;
      }

      csv_file << topic_tv.second.at(i).first << csv_separator << topic_tv.second.at(i).second << csv_separator;
    }

    csv_file << std::endl;

    feedback.progress_msg = "Writing values in the CSV file";
    // Publish progression 60 -> 90%
    const unsigned new_progress_value(60.0 + (100.0 / 3.33 * csv_index_line) / max_bag_lines);
    if (new_progress_value != feedback.progress_value)
    {
      feedback.progress_value = new_progress_value;
      gh.publishFeedback(feedback);
    }
    ++csv_index_line;
  }

  feedback.progress_msg = "Closing CSV file";
  feedback.progress_value = 95;
  gh.publishFeedback(feedback);
  csv_file.close();

  feedback.progress_msg = "Success";
  feedback.progress_value = 100;
  gh.publishFeedback(feedback);
  ros::Duration(0.25).sleep();

  gh.setSucceeded(result);
  return;
}

void exportMergedCsvCancelCb(actionlib::ServerGoalHandle<rqt_bag_exporter::ExportToMergedCsvAction> gh)
{
  if (gh.getGoalStatus().status == actionlib_msgs::GoalStatus::ACTIVE
      || gh.getGoalStatus().status == actionlib_msgs::GoalStatus::PREEMPTING)
    gh.setCanceled();
}

bool openBag(rqt_bag_exporter::OpenBag::Request &req,
             rqt_bag_exporter::OpenBag::Response &res)
{
  bag_file_name.clear();
  if (req.bag_file.find(".bag") == std::string::npos)
  {
    res.error = "Not a bag file";
    return true;
  }

  if (bag)
    bag->close();

  bag.reset(new rosbag::Bag);
  try
  {
    bag->open(req.bag_file, rosbag::bagmode::Read);
  }
  catch (rosbag::BagException &e)
  {
    res.error = e.what();
    return true;
  }

  bag_file_name = req.bag_file;
  return true;
}

bool closeBag(rqt_bag_exporter::CloseBag::Request &,
              rqt_bag_exporter::CloseBag::Response &)
{
  if (!bag || bag->getSize() != 0)
    return true;

  bag->close();
  return true;
}

bool listTopics(rqt_bag_exporter::ListTopics::Request &,
                rqt_bag_exporter::ListTopics::Response &res)
{
  if (!bag || bag->getSize() <= 0)
  {
    res.error = "No valid bag file is opened, call open_bag service to open one.";
    return true;
  }

  rosbag::View view(*bag);
  std::vector<const rosbag::ConnectionInfo *> connection_infos = view.getConnections();

  for (const rosbag::ConnectionInfo *info : connection_infos)
  {
    rqt_bag_exporter::Topic t;
    t.name = info->topic;
    t.type = info->datatype;
    res.topics.push_back(t);
  }
  return true;
}

bool estimateVideoFps(rqt_bag_exporter::EstimateVideoFps::Request &req,
                      rqt_bag_exporter::EstimateVideoFps::Response &res)
{
  if (!bag || bag->getSize() <= 0)
  {
    res.error = "No valid bag file is opened, call open_bag service to open one.";
    return true;
  }

  rosbag::View view(*bag, rosbag::TopicQuery(req.topic_name));
  res.fps = view.size() / (view.getEndTime().sec - view.getBeginTime().sec);
  return true;
}

bool getDuration(rqt_bag_exporter::GetDuration::Request &,
                 rqt_bag_exporter::GetDuration::Response &res)
{
  if (!bag || bag->getSize() <= 0)
  {
    res.error = "No valid bag file is opened, call open_bag service to open one.";
    return true;
  }

  rosbag::View view(*bag);
  res.duration = view.getEndTime().toSec() - view.getBeginTime().toSec();
  return true;
}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "rqt_bag_exporter_servers");
  ros::NodeHandle nh;
  ros::AsyncSpinner spinner(2);
  spinner.start();

  actionlib::ActionServer<rqt_bag_exporter::ExportToCsvAction> action_server_1(nh,
      "export_csv",
      &exportCsv,
      &exportCsvCancelCb,
      false);
  action_server_1.start();

  actionlib::ActionServer<rqt_bag_exporter::ExportToMergedCsvAction> action_server_2(nh,
      "export_to_one_csv",
      &exportMergedCsv,
      &exportMergedCsvCancelCb,
      false);
  action_server_2.start();

  actionlib::ActionServer<rqt_bag_exporter::ExportToVideoAction> action_server_3(nh,
      "export_video",
      &exportVideo,
      &exportVideoCancelCb,
      false);
  action_server_3.start();

  ros::ServiceServer service_1 = nh.advertiseService("open_bag", openBag);
  ros::ServiceServer service_2 = nh.advertiseService("close_bag", closeBag);
  ros::ServiceServer service_3 = nh.advertiseService("list_topics", listTopics);
  ros::ServiceServer service_4 = nh.advertiseService("estimate_video_fps", estimateVideoFps);
  ros::ServiceServer service_5 = nh.advertiseService("get_duration", getDuration);

  ros::waitForShutdown();
  spinner.stop();
  return 0;
}
