#include <rqt_bag_exporter/mainwindow.hpp>
#include <QApplication>

int main(int argc,
         char *argv[])
{
  ros::init(argc, argv, "rosbag_exporter");

  QApplication a(argc, argv);
  rqt_bag_exporter::MainWindow w;
  w.setWindowTitle("Rosbag exporter");
  w.show();
  return a.exec();
}
