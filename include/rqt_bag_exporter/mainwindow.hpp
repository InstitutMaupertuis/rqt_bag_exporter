#ifndef RQT_BAG_EXPORTER_MAINWINDOW_HPP
#define RQT_BAG_EXPORTER_MAINWINDOW_HPP

#include <actionlib/client/simple_action_client.h>
#include <QApplication>
#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QList>
#include <QMainWindow>
#include <QMenuBar>
#include <QMessageBox>
#include <QProgressDialog>
#include <QPushButton>
#include <QScrollArea>
#include <QSettings>
#include <QSpinBox>
#include <QString>
#include <QTabWidget>
#include <QtConcurrent/QtConcurrent>
#include <QWidget>
#include <ros/ros.h>
#include <rqt_bag_exporter/CloseBag.h>
#include <rqt_bag_exporter/EstimateVideoFps.h>
#include <rqt_bag_exporter/ExportToCsvAction.h>
#include <rqt_bag_exporter/ExportToMergedCsvAction.h>
#include <rqt_bag_exporter/ExportToVideoAction.h>
#include <rqt_bag_exporter/GetDuration.h>
#include <rqt_bag_exporter/ListTopics.h>
#include <rqt_bag_exporter/OpenBag.h>
#include <rqt_bag_exporter/topic_types.hpp>

namespace rqt_bag_exporter
{

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = 0);
  ~MainWindow();

Q_SIGNALS:
  void enable(const bool);
  void finishedCurrentDataExport();
  void updateProgressDialogValue(const unsigned progress);
  void updateProgressDialogText(const QString message);
  void displayMessageBox(const QString,
                         const QString,
                         const QString,
                         const QMessageBox::Icon);

private Q_SLOTS:
  void progressDialogCancelled();
  void closeBag();
  void exportNextTopic();
  void exportToDirectory();
  void openBagFile();
  void updateTimeBegin(const double time_end);
  void updateTimeEnd(const double time_begin);
  void updateFps(const int fps);
  void displayMessageBoxHandler(const QString title,
                                const QString text,
                                const QString info = "",
                                const QMessageBox::Icon icon = QMessageBox::Icon::Information);
private:
  enum ActionType
  {
    CSV,
    MERGED_Csv,
    VIDEO
  };

  void displayTopics();
  void fillStartEndTime();
  void fillVideoTab(const std::string topic_name);
  void listTopicsInBagFile();
  void fillTopicsToExport();
  void csvAction();
  void csvDoneCb(const actionlib::SimpleClientGoalState &state,
                 const rqt_bag_exporter::ExportToCsvResultConstPtr &result);
  void csvFeedbackCb(const rqt_bag_exporter::ExportToCsvFeedbackConstPtr &feedback);

  void mergedCsvAction();
  void mergedCsvDoneCb(const actionlib::SimpleClientGoalState &state,
                       const rqt_bag_exporter::ExportToMergedCsvResultConstPtr &result);
  void mergedCsvFeedbackCb(const rqt_bag_exporter::ExportToMergedCsvFeedbackConstPtr &feedback);

  void videoAction();
  void videoDoneCb(const actionlib::SimpleClientGoalState &state,
                   const rqt_bag_exporter::ExportToVideoResultConstPtr &result);
  void videoFeedbackCb(const rqt_bag_exporter::ExportToVideoFeedbackConstPtr &feedback);

  void closeEvent(QCloseEvent *event);
  void load();

  rqt_bag_exporter::ListTopicsResponse list_;
  rqt_bag_exporter::ListTopicsResponse to_export_;
  std::string directory_;
  unsigned fps_;
  ActionType action_type_ = ActionType::CSV;

  ros::NodeHandle nh_;

  using ExportToCsvActionClient = actionlib::SimpleActionClient<rqt_bag_exporter::ExportToCsvAction>;
  using ExportToMergedCsvActionClient = actionlib::SimpleActionClient<rqt_bag_exporter::ExportToMergedCsvAction>;
  using ExportToVideoActionClient = actionlib::SimpleActionClient<rqt_bag_exporter::ExportToVideoAction>;

  std::unique_ptr<ExportToCsvActionClient> csv_ac_;
  rqt_bag_exporter::ExportToCsvGoal csv_goal_;
  std::unique_ptr<ExportToMergedCsvActionClient> merged_csv_ac_;
  rqt_bag_exporter::ExportToMergedCsvGoal merged_csv_goal_;
  std::unique_ptr<ExportToVideoActionClient> video_ac_;
  rqt_bag_exporter::ExportToVideoGoal video_goal_;

  QProgressDialog *progress_dialog_;
  QSettings *settings_;
  QCheckBox *csv_one_file_;
  QComboBox *decimal_separator_;
  QPushButton *all_;
  QPushButton *none_;
  QDoubleSpinBox *time_begin_;
  QDoubleSpinBox *time_end_;
  QTabWidget *video_tab_;
  QWidget *start_end_time_;
  QWidget *topics_container_;
};

}

#endif
