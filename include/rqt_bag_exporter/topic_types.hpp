#ifndef RQT_BAG_EXPORTER_TOPIC_TYPES_HPP
#define RQT_BAG_EXPORTER_TOPIC_TYPES_HPP

#include <string>

namespace rqt_bag_exporter
{

bool isImageTopic(const std::string topic_type);
bool isCsvWritableTopic(const std::string topic_type);

}

#endif
