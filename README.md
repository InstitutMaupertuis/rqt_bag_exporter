[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Overview
This package contains a GUI to help users extract data from bag files.

Main features are:

- Export topics to CSV files, by default each topic goes into one CSV file but it is also possible to export one merged CSV file
- Export H264 encoded videos from `sensor_msgs/Image` and `sensor_msgs/CompressedImage` topics (8 bits images with 1 or 3 channels only).
- Progress bar to display the progress of the export
- Ability to export only a portion of a bag file (eg: from second 25 to second 60)
- Ability to change the video frame rate when exporting the video
- Ability to cancel exporting video/CSV

The software is user oriented so it is very easy to use and features a desktop launcher when installed.

![rqt_bag_exporter](documentation/rqt_bag_exporter.png)

# Dependencies

## rosdep
Install, initialize and update [rosdep](https://wiki.ros.org/rosdep).

# Compiling
Create a catkin workspace and clone the project:

```bash
mkdir -p catkin_workspace/src
cd catkin_workspace/src
git clone https://gitlab.com/InstitutMaupertuis/rqt_bag_exporter.git
cd ..
```

## Resolve ROS dependencies
```bash
rosdep install --from-paths src --ignore-src --rosdistro $ROS_DISTRO -y
```

## Compile
```bash
catkin_make
```

# Using the Qt GUI user application
[Source the catkin workspace](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment#Create_a_ROS_Workspace) in which you compiled the package, then launch:

```bash
roslaunch rqt_bag_exporter gui.launch
```

There is desktop a file installed when installing the project: you can open the software like any other application using the application menu.

![Desktop launcher](documentation/desktop_launcher.png)

:information_source: If you delete the installed files from your workspace the desktop launcher will not be deleted. It is located in `$HOME/.local/share/applications`; you need to remove this file manually.

# Multi bag exporter
```bash
roslaunch rqt_bag_exporter multi_bag_exporter.launch dir:="/home/victor/bags" topic_name_include:="/topic_test" merged_csv:=false
```
A full documentation of arguments can be found in the launch file [`launch/multi_bag_exporter.launch`](./launch/multi_bag_exporter.launch)
